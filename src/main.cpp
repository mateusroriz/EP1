#include <sys/types.h>

#include <sys/socket.h>

#include <netinet/in.h>

#include <netdb.h>

#include <arpa/inet.h>

#include <unistd.h>

#include <cstring>

#include <string>

#include <ostream>

#include <fstream>

#include <openssl/rsa.h>

#include <iostream>

#include "network.hpp"

#include "crypto.hpp"

#include "array.hpp"


using namespace std;

int main(int argc, const char **argv){

    int FD;
    
    RSA * private_key = crypto::rsa_read_private_key_from_PEM("private.pem");
    
    array::array *pedido_de_registro = array::create(7);
    
    array::array *inicio_registro = array::create(7);
    
    array::array *ID_person = array::create(8);



    pedido_de_registro->data[0] = 0x03;
    pedido_de_registro->data[1] = 0;
    pedido_de_registro->data[2] = 0;
    pedido_de_registro->data[3] = 0;
    pedido_de_registro->data[4] = 0xC0;
    pedido_de_registro->data[5] = 0;
    pedido_de_registro->data[6] = 0;

    ID_person->data[0] = 0x4e; 
    ID_person->data[1] = 0xac;    
    ID_person->data[2] = 0x16;
    ID_person->data[3] = 0x6b;
    ID_person->data[4] = 0x4b;    
    ID_person->data[5] = 0x50;
    ID_person->data[6] = 0x11;
    ID_person->data[7] = 0xc3;
    

    
    RSA *chave_servidor;

    chave_servidor = crypto::rsa_read_public_key_from_PEM("server_pk.pem");

    array::array *ID_criptog;

    ID_criptog = crypto::rsa_encrypt(ID_person, chave_servidor);

    array::array* hash_ID_criptog;

    hash_ID_criptog = crypto::sha1(ID_criptog);

    cout << *ID_criptog << endl; 

    array::array *Registrado;

    array::array *pacote = array::create(535);

    pacote->data[0] = 0xC2;
    pacote->data[1] = 0x00;
    pacote->data[2] = 0x02;

    memcpy(pacote->data + 3, ID_criptog->data, 512);

    memcpy(pacote->data + 515, hash_ID_criptog->data, 20);

    array::array *registrar_id = array::create(539);

    registrar_id->data[0] = 0x17; 
    registrar_id->data[1] = 0x02;
    registrar_id->data[2] = 0;
    registrar_id->data[3] = 0;

    memcpy(registrar_id->data +4, pacote->data,535);

    cout << *registrar_id << endl;

    cout << "iniciando a comunicaçao com o servidor" << endl;
    



    FD = network::connect("45.55.185.4",3000);

    if(FD < 0){
        cout << "Falha na conexão" << FD << endl;
    }else {
        cout << "Conexao OK" << endl;
    }
        network::write(FD, pedido_de_registro);
        if((inicio_registro = network::read(FD)) == nullptr){
            cout << "Leitura Nula" << endl;
        }
        else{
            cout << "Imprimindo conteudo do pacote recebido: inicio_registro" << inicio_registro -> length << "]" << endl;
            for(int i = 0; i < ((int) inicio_registro->length);i++) {
                printf("%X",inicio_registro->data[i]);
            }
            sleep(2);
            printf("\n");
        }
      
    network::write(FD,registrar_id);

    if((Registrado = network::read(FD))== nullptr){
        cout << "Leitura Nula" << endl;
    }    
    else{
        cout << "imprimindo conteudo do pacote recebido: Registrado" << Registrado->length << endl;
        for(int i = 0; i < ((int) Registrado->length);i++) {
            printf("%X",Registrado->data[i]);
        }
        sleep(2);
        printf("\n");
    }

    cout << "Tamanho: " << Registrado->length << endl;

    array::destroy(hash_ID_criptog);

    array::destroy(pedido_de_registro);



    array::array *s_key = array::create(512);

    memcpy(s_key->data, Registrado->data + 7, 512);

    array::array *s_key_descript = crypto::rsa_decrypt(s_key,private_key);


    
    cout << "Imprimindo conteudo do pacote s_key_descript "<< s_key_descript -> length << endl;
          for (int i = 0; i < ((int) s_key_descript->length); i++)
          {
            printf("%X ", s_key_descript->data[i]);
          }
          printf("\n");

    FILE* arquivo = fopen("chave_s.pem","w+");
      for(int i = 0; i < ((int) s_key_descript->length); i++) {
        fprintf(arquivo, "%X", s_key_descript->data[i]);
      }
      fclose(arquivo);  




    RSA * chave_servidor2;

    chave_servidor2 = crypto::rsa_read_public_key_from_PEM("server_pk.pem");

    array::array* ID_criptog2;

    ID_criptog2 = crypto::rsa_encrypt(ID_person, chave_servidor2);

    array::array* hash_ID_criptog2;

    hash_ID_criptog2 = crypto::sha1(ID_criptog2);

    cout << *ID_criptog2 << endl;

    array::array *auth_start;

    array::array *pacote1;

    pacote1 = array::create(535);

    pacote1-> data[0] = 0xA0;

    pacote1-> data[1] = 0x00;

    pacote1-> data[2] = 0x02;



    memcpy(pacote1->data + 3, ID_criptog2->data,512);

    memcpy(pacote1->data + 515, hash_ID_criptog2->data, 20);
 
    array::array *pedido_autent = array::create(539);
    
    pedido_autent->data[0] = 0x17;
    pedido_autent->data[1] = 0x02;
    pedido_autent->data[2] = 0;
    pedido_autent->data[3] = 0;

    memcpy(pedido_autent->data +4, pacote1->data, 535);

    cout << *pedido_autent << endl;

    if(FD < 0){
      cout << "Falha na conexao" << endl; 
      return 1; 
    } 
    else{
        network::write(FD, pedido_autent);
        if((auth_start = network::read(FD))==nullptr){
          cout << "Leitura Nula" << endl;
        }
        else{
          cout << "Imprimindo conteudo do pacote auth_start "<< auth_start -> length << endl;
          for (int i = 0; i < ((int) auth_start->length); i++){
            printf("%X ", auth_start->data[i]);
          }
          printf("\n");
        }
    }

    array::array* token_A_descriptografado;

    array::array* token_A_encript = array::create(512);

    memcpy(token_A_encript->data, auth_start->data + 7, 512);

    token_A_descriptografado = crypto::rsa_decrypt(token_A_encript, private_key);


    array::array *desafio;

    array::array *desafio_requisicao = array::create(7);

    desafio_requisicao->data[0] = 0x03;
    desafio_requisicao->data[1] = 0;
    desafio_requisicao->data[2] = 0;
    desafio_requisicao->data[3] = 0;
    desafio_requisicao->data[4] = 0xA2;
    desafio_requisicao->data[5] = 0;
    desafio_requisicao->data[6] = 0;

    network::write(FD, desafio_requisicao);
      if((desafio = network::read(FD))==nullptr){
          cout << "Leitura Nula" << endl;
      }
      else{
        cout << "Imprimindo conteudo do pacote desafio "<< desafio -> length << endl;
        for (int i = 0; i < ((int) desafio->length); i++){
            printf("%X ", desafio->data[i]);
        }
          printf("\n");
      }





    array::array *M_encript = array::create(32);

    memcpy(M_encript->data, desafio->data + 7, 32);

    array::array *M_descript;

    M_descript = crypto::aes_decrypt(M_encript, token_A_descriptografado, s_key_descript);

    cout << "M_descript crypt " << *M_descript << endl;

    array::array* hash_Mem;

    hash_Mem = crypto::sha1(M_descript);

    
    array::array *authenticated;

    array::array *pacote2;
    
    pacote2 = array::create(39);
    pacote2-> data[0] = 0xA5;
    pacote2-> data[1] = 0x10;
    pacote2-> data[2] = 0;




    memcpy(pacote2->data + 3, M_descript->data,16);

    memcpy(pacote2->data + 19, hash_Mem->data, 20);
   
    array::array *authenticate = array::create(43);
    
    authenticate->data[0] = 0x27;
    authenticate->data[1] = 0;
    authenticate->data[2] = 0;
    authenticate->data[3] = 0;

    memcpy(authenticate->data +4, pacote2->data, 39);

    cout << "Pacote authenticate " << *authenticate << endl;

    if(FD < 0){
      cout << "Falha na conexao" << endl; 
      return 1; 
    } 
    else{
        network::write(FD, authenticate);
        if((authenticated = network::read(FD))==nullptr){
            cout << "Leitura Nula" << endl;
        }
        else{
            cout << "Imprimindo conteudo do pacote authenticated "<< authenticated -> length << endl;
            for (int i = 0; i < ((int) authenticated->length); i++){
              printf("%X ", authenticated->data[i]);
            }
            printf("\n");
        }
    }

    array::array *Tok_criptogr = array::create(59);

    memcpy(Tok_criptogr->data, authenticated->data + 7, 59);

    array::array* token_t_desc;

    token_t_desc = crypto::aes_decrypt(Tok_criptogr, token_A_descriptografado, s_key_descript);

    array::destroy(token_A_descriptografado);

    cout << "Token T " << *token_t_desc << endl;

    array::array* ID_objeto = array::create(8);
    ID_objeto->data[0] = 0x01;
    ID_objeto->data[1] = 0x02;
    ID_objeto->data[2] = 0x03;
    ID_objeto->data[3] = 0x04;
    ID_objeto->data[4] = 0x05;
    ID_objeto->data[5] = 0x06;
    ID_objeto->data[6] = 0x07;
    ID_objeto->data[7] = 0x08;

    array::array* ID_objeto_encript;

    ID_objeto_encript = crypto::aes_encrypt(ID_objeto, token_t_desc, s_key_descript);

    cout << "Id do objeto Encriptado " << *ID_objeto_encript << endl;

    array::array* hash_ID_objeto;

    hash_ID_objeto = crypto::sha1(ID_objeto_encript);


    array::array *objeto2;

    array::array* pacote3;
    
    pacote3 = array::create(39);
    pacote3-> data[0] = 0xB0;
    pacote3-> data[1] = 0x10;
    pacote3-> data[2] = 0;

    memcpy(pacote3->data + 3, ID_objeto_encript->data,16);

    memcpy(pacote3->data + 19, hash_ID_objeto->data, 20);
   
    array::array *pedido_objeto = array::create(43);
    pedido_objeto->data[0] = 0x27;
    pedido_objeto->data[1] = 0;
    pedido_objeto->data[2] = 0;
    pedido_objeto->data[3] = 0;

    memcpy(pedido_objeto->data +4, pacote3->data, 39);

    cout << "Pacote Request que esta sendo enviado " << *pedido_objeto << endl;

    if(FD < 0){
      cout << "Falha na conexao" << endl; 
      return 1; 
    } 

    else{
      network::write(FD, pedido_objeto);
      if((objeto2 = network::read(FD,11447))==nullptr){
          cout << "Leitura Nula" << endl;
      }
      else{
          cout << "Imprimindo conteudo do pacote objeto2 "<< objeto2 -> length << endl;
          for (int i = 0; i < ((int) objeto2->length); i++){
              printf("%X ", objeto2->data[i]);
          }
            printf("\n");
      }
    }
    
    array::array *objeto_criptografado = array::create(11447);
    objeto_criptografado->data[0] = 0x2c;
    objeto_criptografado->data[1] = 0xb7;
    objeto_criptografado->data[2] = 0;
    objeto_criptografado->data[3] = 0; 

    memcpy(objeto_criptografado->data, objeto2->data + 7, 11447);

    cout << *objeto_criptografado << endl;

    array::array *objec;

    objec = crypto::aes_decrypt(objeto_criptografado, token_t_desc, s_key_descript);

    cout << "Conteudo Final " << *objec << endl; 

    ofstream arquivo3;
    arquivo3.open ("imagem.jpeg", ios::binary);
        for(int i=0; i <((int)objec->length); i++){
arquivo3 <<objec->data[i];
        }

arquivo3.close();
    return 0;
}